package pl.edu.agh.com.wprowadzeniedoandroid;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button bt = findViewById(R.id.openButton);
        bt.setEnabled(false);

        final EditText et = findViewById(R.id.urlText);

        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et.setText("");
                bt.setEnabled(true);
            }
        });
    }

    public void openBrowser(View view) {
        UrlValidator urlValidator = new UrlValidator();

        final EditText et = findViewById(R.id.urlText);
        String url = urlValidator.validate(et.getText().toString());
        if (url == null) {
            createSimpleErrorToast();
        } else {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
    }

    private void createSimpleErrorToast() {
        Context context = getApplicationContext();
        CharSequence text = "Wrong URL, please write again!";
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(Gravity.TOP, 0, 0);

        toast.show();
    }
}
