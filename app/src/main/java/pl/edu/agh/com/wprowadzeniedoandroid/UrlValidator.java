package pl.edu.agh.com.wprowadzeniedoandroid;

import java.util.regex.Pattern;

import lombok.NoArgsConstructor;

@NoArgsConstructor
class UrlValidator {

    private static final String URL_RE = "(https?://)?[\\w]+(\\.\\w+)+(/.*)?";

    String validate(String url) {
        url = url.trim();
        Pattern pattern = Pattern.compile(URL_RE);
        if (!pattern.matcher(url).find())
            return null;
        return url.contains("http://") || url.contains("https://")
                ? url
                : "http://" + url;
    }
}
