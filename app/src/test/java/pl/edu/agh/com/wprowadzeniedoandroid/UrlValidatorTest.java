package pl.edu.agh.com.wprowadzeniedoandroid;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class UrlValidatorTest {

    private UrlValidator urlValidator;

    @Before
    public void setUp() {
        urlValidator = new UrlValidator();
    }

    @Test
    public void validate_whenInvoked_shouldReturnUrl() {
        //given
        String url = "http://google.com/";

        //when
        String result = urlValidator.validate(url);

        //then
        assertNotNull(result);
        assertEquals(url, result);
    }

    @Test
    public void validate_whenInvokedWithoutHttp_shouldReturnUrl() {
        //given
        String url = "google.com";
        String expected = "http://google.com";

        //when
        String result = urlValidator.validate(url);

        //then
        assertNotNull(result);
        assertEquals(expected, result);
    }

    @Test
    public void validate_whenInvokedWithWrongUrl_shouldReturnNull() {
        //given
        String url = "whatWillHappenNow?";

        //when
        String result = urlValidator.validate(url);

        //then
        assertNull(result);
    }
}